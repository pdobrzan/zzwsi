﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZZWSI
{
    class Producer
    {
        public bool IsFinished;
        private Queue<Frame> BufferQueue;
        public int BufferSize;

        public Producer(Queue<Frame> buffer, int size)
        {
            BufferQueue = buffer;
            IsFinished = false;
            BufferSize = size;
        }

        public void Produce()
        {
            lock (((ICollection)BufferQueue).SyncRoot)
            {
                for (int i = 1; i < 15; i++)
                {
                    Frame val = GetFrame(i);
                    BufferQueue.Enqueue(val);

                    if (BufferQueue.Count == BufferSize)
                    {
                        Monitor.PulseAll(((ICollection)BufferQueue).SyncRoot);
                        Monitor.Wait(((ICollection)BufferQueue).SyncRoot);
                    }
                }
            }
            
            while (BufferQueue.Count > 0)
            {
                lock (((ICollection)BufferQueue).SyncRoot)
                {
                    Monitor.PulseAll(((ICollection)BufferQueue).SyncRoot);
                }
            }
            IsFinished = true;
            Console.WriteLine("Producent zakończył pracę.");
        }

        private Frame GetFrame(int i)
        {
            string name = "Brain MRI - Multiple Sclerosis " + i.ToString().PadLeft(4, '0') + ".jpg";
            Image<Gray, Byte> img = new Image<Gray, Byte>("video/" + name);
            Frame val = new Frame(name, img);
            Console.WriteLine("Wyprodukowano element.");
            return val;
        }
    }
}
