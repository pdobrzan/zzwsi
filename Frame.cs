﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZZWSI
{
    public class Frame
    {
        public string Name;
        public Image<Gray, Byte> Data;

        public Frame(string name, Image<Gray, Byte> data)
        {
            Name = name;
            Data = data;
        }
    }
}
