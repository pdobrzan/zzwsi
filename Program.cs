﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections;
using Emgu.CV;
using Emgu.CV.Structure;
using System.Drawing;
using AForge.Video;
using System.Drawing.Imaging;
using System.Net;

namespace ZZWSI
{
    class Program
    {
        private static void Main(string[] args)
        {
            int threads = 6;
            int BufferSize = 6;
            Queue<Frame> buffer = new Queue<Frame>();
            Producer producer = new Producer(buffer, BufferSize);

            Consumer[] consumerTab = new Consumer[threads];
            Thread[] threadTab = new Thread[threads];

            System.Diagnostics.Stopwatch sw = new System.Diagnostics.Stopwatch();
            sw.Start();
            Thread producerThread = new Thread(new ThreadStart(producer.Produce));
            producerThread.Start();


            for (int i = 0; i < threads; i++)
            {
                consumerTab[i] = new Consumer(buffer, producer, i);
                threadTab[i] = new Thread(new ThreadStart(consumerTab[i].Consume));
                threadTab[i].Start();
            }
            
            for (int i = 0; i < threads; i++)
            {
                threadTab[i].Join();
            }

            sw.Stop();
            Console.WriteLine("Upłynęło " + sw.Elapsed.TotalMilliseconds.ToString() + "ms.");

            Console.Read();
        }
    }
}
