﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ZZWSI
{
    class Consumer
    {
        private Queue<Frame> BufferQueue;
        private Producer producer;
        private int id;

        public Consumer(Queue<Frame> buffer, Producer producer, int _id)
        {
            BufferQueue = buffer;
            this.producer = producer;
            id = _id;
        }

        public void Consume()
        {
            Frame currentElement;
            while (!producer.IsFinished || BufferQueue.Count != 0)
            {
                lock (((ICollection)BufferQueue).SyncRoot)
                {
                    if (BufferQueue.Count == 0)
                    {
                        if (producer.IsFinished) break;
                        Monitor.Wait(((ICollection)BufferQueue).SyncRoot);
                        if (producer.IsFinished) break;
                    }

                    currentElement = BufferQueue.Dequeue();
                    Console.WriteLine("Konsument numer " + id.ToString() + " pobrał element.");

                    if (BufferQueue.Count == 0)
                    {
                        if (producer.IsFinished) break;
                        Monitor.Pulse(((ICollection)BufferQueue).SyncRoot);
                    }
                }
                FilterAndSave(currentElement);
            }
            Console.WriteLine("Konsument numer " + id + " zakończył pracę.");
        }

        private void FilterAndSave(Frame currentElement)
        {
            Image<Gray, Byte> img = currentElement.Data;
            Image<Gray, Byte> tmp = currentElement.Data;
            string name = currentElement.Name;
            CvInvoke.EqualizeHist(img, img);
            CvInvoke.MedianBlur(img, img, 5);
            CvInvoke.MedianBlur(img, img, 7);
            CvInvoke.MedianBlur(img, img, 9);
            CvInvoke.MedianBlur(img, img, 11);
            CvInvoke.MedianBlur(img, tmp, 19);
            tmp.Save("out/" + name);
        }
    }
}
